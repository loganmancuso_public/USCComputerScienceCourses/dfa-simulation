# /****************************************************************
#  * 'build.sh'
#  *
#  * Author/CopyRight: Mancuso, Logan
#  * Last Edit Date: 11-27-2018--13:30:58
#  *
#  * This script will run the program, run from
#  * WorkingDirectory/SourceFiles/
# **/

#!bin/bash

# /****************************************************************
#  * Error Messages
# **/

# WRONG_DIRECTORY="Wrong Directory, Move To Working Directory To Run Program"
YES_OR_NO="Invalid please type [Y]es or [N]o"
IOFiles="../../IOFiles"

if [[ ! $PWD/ = */WorkingDirectory/SourceFiles/ ]]; then
  cd WorkingDirectory/SourceFiles
fi
touch $IOFiles/sh.log 
echo "Executing zbBackupProgram.sh" 
echo "-----------------------------------------------------------------" 
../../.Executable/./zbBackupProgram.sh 
echo "Executing zcCopyProgram.sh" 
echo "-----------------------------------------------------------------" 
../../.Executable/./zcCopyProgram.sh 
echo "Executing zdCompile.sh" 
echo "-----------------------------------------------------------------" 
../../.Executable/./zdCompile.sh 2> $IOFiles/sh.log