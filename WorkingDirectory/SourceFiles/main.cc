#include "main.h"
/****************************************************************
 * 'main.cc'
 * Main file for program
 *
 * Author/CopyRight: Mancuso, Logan
 * Last Edit Date: 11-27-2018--13:30:41
 *
**/

static const std::string kTag = "Main: ";

int main(int argc, char *argv[]) {
  //variables for files
	std::string flag = "XX";
  std::string in_filename_1 = "XX";
  std::string in_filename_2 = "XX";

  Scanner input_stream01;
  Scanner input_stream02;

  //check arguments and store as variables
	flag = static_cast<std::string>( argv[1] );
	if ( flag == "-S" ) {
		Utils::CheckArgs(3, argc, argv, "flag infilename01 infilename02 ");
		in_filename_1 = static_cast<std::string>( argv[2] );
		in_filename_2 = static_cast<std::string>( argv[3] );
		input_stream01.OpenFile( in_filename_1 );
		input_stream02.OpenFile( in_filename_2 );
	} else if ( flag == "-M" ) { 
		Utils::CheckArgs(2, argc, argv, "flag infilename01");
		in_filename_1 = static_cast<std::string>( argv[2] );
		input_stream01.OpenFile( in_filename_1 );
	} else {
    std::cout << "invalid flag provided" << std::endl;
		exit(1);
	}

	//open input file
	Simulation simulation;
	simulation.read( input_stream01, 0);
	simulation.dfa.print_dfa();

  if( flag == "-S" ) {
		simulation.read( input_stream02, 1);
		simulation.dfa.print_simulation();
	} else {
		simulation.read( input_stream02, 2 );
		simulation.dfa.print_minimization();
	}//end

   
  return 0;
}
/****************************************************************
 * End 'main.cc'
**/

