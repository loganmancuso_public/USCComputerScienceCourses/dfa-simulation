#include "simulation.h"
/****************************************************************
  * 'simulation.cc'
  *
  * Author/CopyRight: Mancuso, Logan
  * Last Edit Date: 11-27-2018--13:30:41
  *
 **/

static const std::string kTag = "Simulation: ";


Simulation::Simulation() {}//constructor
Simulation::~Simulation() {}//de-constructor


 /****************************************************************
  * Reading the configuration file and building DFA
  * read
  * set_dfa
 **/

void Simulation::read(  Scanner& data_stream, int file ) {
 int line_num = 0;
  while( data_stream.HasNext() ) {
    std::string line = data_stream.NextLine();
    if( file == 0 ) {
      set_dfa( line_num, line );
    } else if( file == 1 ) {
      line.pop_back();
      dfa.in_language( line );
    } else if( file == 2) {
      minimize( line );
    }
    line_num++;
  }//end while
}//end read

//Setting the data given from the DFA config file
void Simulation::set_dfa( int line_num, const std::string& input) {
  ScanLine scanline;
  std::vector<std::string> input_line;
  scanline.OpenString(input);
	std::string alphabet;
	

	if ( line_num != 2 ) {
		while( scanline.HasNext() ) {
			std::string word = scanline.Next();
			input_line.push_back(word);
		}//end while
	} else {
		alphabet = scanline.NextLine();
		alphabet.erase(0,10);
	}

  if ( line_num == 0 ) { //Number of states
    dfa.set_number_of_states( std::stoi(input_line.at( 3 ) ) );    
  } else if ( line_num == 1 ) { //Accepting states
	  dfa.set_final_states( input_line );
  } else if ( line_num == 2 ) { //Alphabet
    dfa.set_alphabet( alphabet );
  } else { //transition table
    // each line is a set of vectors
    std::vector<int> line = {};
    for( uint i=0; i<input_line.size(); i++ ) {
      std::copy(input_line.at(i).begin(),input_line.at(i).end(), std::back_inserter(line));
    }
    for( uint i=0; i<line.size(); i++ ) {
			char a = dfa.get_alphabet_at(i);
			int b = line.at(i) - '0';
			dfa.set_transition_table( state, a , b );
    }//end for
    state++;		
  }//end else
}//end set_dfa

 /****************************************************************
  * Computation.
  * given an input string check to see if this is valid
  * minimize
 **/

void Simulation::minimize( const std::string& input ) {

}//end minimize


 /****************************************************************
  * To String formatting functions
 **/

 /****************************************************************
  * End 'simulation.cc'
 **/
