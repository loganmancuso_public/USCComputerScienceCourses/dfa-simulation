#include "dfa.h"
/****************************************************************
  * 'dfa.cc'
  *
  * Author/CopyRight: Mancuso, Logan
  * Last Edit Date: 11-27-2018--13:30:41
	* 
 **/

static const std::string kTag = "DFA: ";
static const std::string new_line = "\n";
static const std::string accepted_ = "was accepted by the DFA";
static const std::string rejected_ = "was rejected by the DFA";


DFA::DFA() {}//constructor
DFA::~DFA() {}//de-constructor

/****************************************************************
  * Mutators
 **/
void DFA::set_number_of_states( int states ) {
	if( states != 0 ) {
    this->number_of_states_ = states;
	} else {
		std::cout << "number of states <= 0 exiting" << std::endl;
		exit(1);
	}
}
void DFA::set_final_states( std::vector<std::string> string ) {
	int off_set = 2;
  for( uint i=off_set; i<=string.size()-off_set; i++ ) {
		this->final_states_.push_back( std::stoi( string.at(i)));
  }
}
void DFA::set_alphabet( std::string string ) {
	std::copy(string.begin(), string.end(), std::back_inserter(this->alphabet_));
  this->alphabet_.pop_back();
}
void DFA::set_transition_table( int state, char c, int trans ) { //state -> reading c = transition [state,c] = t
  this->transition_table_.insert( std::make_pair( std::make_pair( state, c ) , trans  ) );
}
void DFA::set_language( std::string input, bool result ) {
	std::string insert;
	(result) ? insert = accepted_ : insert = rejected_;
  this->language_.push_back( std::make_pair( input, insert ) );
}
void DFA::set_minimized_transition_table( int state, char c, int trans ) {
  this->minimized_transition_table_.insert( std::make_pair( std::make_pair( state, c ) , trans  ) );
}

/****************************************************************
  * Acessors 
 **/
int DFA::get_number_of_states() {
  return this->number_of_states_;
}
std::vector<int> DFA::get_final_states() {
	return this->final_states_;
}
std::vector<char> DFA::get_alphabet() {
	return this->alphabet_;
}
char DFA::get_alphabet_at( int pos ) {
	return this->alphabet_.at(pos);
} 
std::map<std::pair<int, char>, int> DFA::get_transition_table() {
	return this->transition_table_;
}
std::vector<std::pair<std::string, std::string>> DFA::get_language() {
	return this->language_;
}
std::map<std::pair<int, char>, int> DFA::get_minimized_transition_table() {
	return this->minimized_transition_table_;
}

/****************************************************************
  * is part of the language in the DFA.
 **/
void DFA::in_language( std::string input ) {
  bool la = false;
  int state = 0;
	for( char c : input ) {
    state = this->transition_table_.find(std::make_pair(state,c))->second;
		//std::cout << "state: " + std::to_string(state) << std::endl; 
	}//follow the chars through the map of pairs to each transition state
	for( auto& val:this->final_states_ ) {
    if( val == state ) {
			la = true;
			break;
		}
	}
  //std::cout << std::boolalpha << la << std::endl;
  set_language( input, la ); //add to vector
}//end in_language


/****************************************************************
  * Print functions
	* print dfa
	* print simulation
	* print minimization
 **/
std::string DFA::print_dfa() {
	std::string to_return = "\nPrinting DFA configuration:\n";
  to_return  += "Number of States: " + 
  std::to_string( get_number_of_states() ) + new_line;
  to_return += "Accepting States: ";
  for( const auto& row:get_final_states() ) {
    to_return += std::to_string(row) + " ";
  }  
	to_return += "\nAlphabet: ";
  for( const auto& row:get_alphabet() ) {
	  to_return += "\'" + std::string(1,row) + "\' ";
  }
  to_return += "\nTransition Table:\n";
	for( const auto& row:get_transition_table() ) {
    to_return += "{" + std::to_string(row.first.first) 
		+ ",\'" + std::string(1,row.first.second) + "\'}\t" 
		+ std::to_string(row.second) + new_line;
	}
  to_return += "\n----------------------------\n";
  std::cout << to_return << std::endl;
  return to_return;
}//end print_dfa

std::string DFA::print_simulation() {
  std::string to_return = "Simulation:\n";
  for( const auto& row:get_language() ) {
    to_return += "\"" + row.first + "\"\t" 
		+ row.second  + new_line; 
  }
	to_return += "\n----------------------------\n";
  std::cout << to_return << std::endl;
  return to_return;
}//end print_simulation
  
std::string DFA::print_minimization() {
  std::string to_return = "\nMinimized Transition Table:";
	for( const auto& row:get_minimized_transition_table() ) {
    to_return += "{" + std::to_string(row.first.first) 
		+ ",'" + std::string(1,row.first.second) 
		+ "'} = " + std::to_string(row.second) + new_line;
	}
  std::cout << to_return << std::endl;
  return to_return;
}
/****************************************************************
  * End 'dfa.cc'
 **/
