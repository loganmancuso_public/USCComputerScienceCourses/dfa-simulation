/****************************************************************
  * 'simulation.h'
  *
  * Author/CopyRight: Mancuso, Logan
  * Last Edit Date: 11-27-2018--13:30:41
  *
 **/

#ifndef SIMU_H
#define SIMU_H

#include <iostream>
#include <string>
#include <vector>

#include "../../Utilities/scanner.h"
#include "../../Utilities/scanline.h"
#include "dfa.h"

class Simulation {
  public:
  //construct/deconstruct
    Simulation();
    virtual ~Simulation();
  //functions 
    DFA dfa;
    void read( Scanner& input, int file );

  private:
    int state = 0;
    void set_dfa( int line_num, const std::string& input );
    void simulate( const std::string& input );
    void minimize( const std::string& input );
    std::string print_simulation( std::vector<std::string> values );
};//end class

#endif //SIMU_H

 /****************************************************************
  * End 'simulation.h'
 **/
