# *************************************************************
# 'makefile'
# This is a generic 'makefile' for any program
#
# Author/CopyRight: Mancuso, Logan
# Last Edit Date: 11-27-2018--13:30:52
# *************************************************************

# use compiler c++ version 17
GPP = g++
FLAG = -O3 -Wall -std=c++11 #i prefer 17 but not sure what school computers have
# include utilities folder
UTILS = ../../Utilities

M = main.o
S = scanner.o
SL = scanline.o
U = utils.o
SI = simulation.o
DFA = dfa.o


FILES = $M $S $(SL) $U $(SI) $(DFA)

# output of Aprog program
Aprog: $(FILES)
	$(GPP) $(FLAG) -o Aprog $(FILES)

# main.cc file
main.o: main.h main.cc
	$(GPP) $(FLAG) -c main.cc

# scanner.cc utils file
scanner.o: $(UTILS)/scanner.h $(UTILS)/scanner.cc
	$(GPP) $(FLAG) -c $(UTILS)/scanner.cc

# scanline.cc utils file
scanline.o: $(UTILS)/scanline.h $(UTILS)/scanline.cc
	$(GPP) $(FLAG) -c $(UTILS)/scanline.cc

# utils.cc utils file
utils.o: $(UTILS)/utils.h $(UTILS)/utils.cc
	$(GPP) $(FLAG) -c $(UTILS)/utils.cc

# *************************************************************
# Other files to compile in this program, "non-default" files
# *************************************************************

simulation.o: simulation.h simulation.cc
	$(GPP) $(FLAG) -c simulation.cc

dfa.o: dfa.h dfa.cc
	$(GPP) $(FLAG) -c dfa.cc

# clean
clean:
	rm -rf *.o Aprog

# *************************************************************
# End 'makefile'
# *************************************************************




